const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

function updateFullName(event){

	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
};

txtLastName.addEventListener('keyup', updateFullName);

txtFirstName.addEventListener('keyup', updateFullName);