import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import './App.css';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { useState } from 'react';
import { UserProvider } from './UserContext';

export default function App() {

  // State hook for the user state that's defined here is for a global scope.
  // To identify whether a user is already logged in or not, by means of localStorage
  const [ user, setUser ] = useState({
    email: localStorage.getItem('email')
  });

  console.log(user);

  const unsetUser = () => {
    localStorage.clear();
  };

  return (
    // A common pattern in React for the component to return multiple elements
    <>
    <UserProvider value={{ user, setUser, unsetUser }} >
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
    </>
  );
}