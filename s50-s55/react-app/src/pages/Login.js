import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Login(props){

	// Allows us to consume the User Context object and it's properties to use for user validation.
	const { user, setUser } = useContext(UserContext);

	const navigate = useNavigate();

	const [ email, setEmail ] = useState("");
	const [ password, setPassword ] = useState("");

	const [ isActive, setIsActive ] = useState(false);

	function authenticate(e) {
		e.preventDefault();

		setEmail("");
		setPassword("");

		// Sets the email of the authenticated user in the local storage
		localStorage.setItem('email', email);

		// Sets the global user state to have properties obtained from the local storage.
		setUser({email: localStorage.getItem("email")});

		alert(`"${email}" has been verified! Welcome back!`);

		//navigate("/courses");
	};

	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [ email, password ])

	return (
		(user.email !== null) ?
			<Navigate to="/courses" />
		:
			<Form onSubmit={(e) => authenticate(e)}>
			<h1>Registration Form</h1>
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email here"
						onChange={(e) => setEmail(e.target.value)}
						value={email}
						required
					/>
				</Form.Group>
				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter password here"
						onChange={(e) => setPassword(e.target.value)}
						value={password}
						required
					/>
				</Form.Group>
				{
					isActive ?

					<Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>
					:
					<Button variant="danger my-3" type="submit" id="submitBtn">Submit</Button>
				}
							
			</Form>
	)
}