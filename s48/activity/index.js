// Create a mock database
let posts = [];

// Posts ID
let count = 1;

// Add Post Data

document.querySelector("#form-add-post").addEventListener('submit', (e) => {

	// Prevents the page from reloading. Also prevents the defalt behavior of our event
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector("#txt-body").value
	});

	count++;

	showPosts(posts);
	alert("Successfully added");

});

// Show Posts

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id = "post-${post.id}">
				<h3 id = "post-title-${post.id}">${post.title}</h3>
				<p id = "post-body-${post.id}">${post.body}</p>
				<button onclick = "editPost(${post.id})">Edit</button>
				<button onclick = "deletePost(${post.id})">Delete</button>
			</div>
		`
	});

	document.querySelector("#div-posts-entries").innerHTML = postEntries;
}

// Edit Post
// function called editPost() that takes a single argument called 'id'
const editPost = (id) => {
	console.log(id);

	// The function first uses the querySelector() method to get the element with the id and assigns its innerHTML property to the title variable same with the body
	// innerHTML - sets or returns HTML element
	let title = document.querySelector(`
		#post-title-${id}`).innerHTML;
	let body = document.querySelector(`
		#post-body-${id}`).innerHTML; 

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
};

const deletePost = (id) => {
	
	for(let i = 0; i< posts.length; i++){

		if(posts[i].id == id){

			posts.splice(i, 1);

			break;
		}
	}

	const element = document.getElementById(`post-${id}`);
	element.remove();
	alert("Successfully deleted");
};

// Update Post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	for(let i = 0; i< posts.length; i++){

		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			alert('Successfully updated!');
			showPosts(posts);

			break;
		}
	}
})